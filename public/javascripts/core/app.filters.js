/**
 * Filter files are used to filter and format data for the view
 * it contains formating and filtering code
 * 
 */

/**
 * This filter is used to format date to user in local format
 * 
 */
(function () {
    'use strict';
    angular.module('app').filter('dateFormatFilter', dateFormatFilter);

    function dateFormatFilter() {
        return function (input) {
            var deadlineDate = new Date(input);
            return deadlineDate.toLocaleDateString();
        };
    }
})();

/**
 * This filter is used to check deadline date
 * if its past date then sets deadlineStatus as -1
 * else if its todays date then sets deadlineStatus as 0
 * else if its next date then today then sets deadlineStatus as 1
 * 
 */
(function () {
    'use strict';
    angular.module('app').filter('deadlineCheckFilter', deadlineCheckFilter);

    function deadlineCheckFilter() {
        return function (input) {
            var deadlineDate = new Date(input);
            deadlineDate.setHours(0, 0, 0, 0);
            var currentDate = new Date();
            currentDate.setHours(0, 0, 0, 0);
            var deadlineStatus = 0;
            if (deadlineDate < currentDate) {
                deadlineStatus = -1;
            } else {
                if (deadlineDate.toString() === currentDate.toString()) {
                    deadlineStatus = 0;
                } else {
                    deadlineStatus = 1;
                }
            }
            return deadlineStatus;
        };
    }
})();