/**
 * Factories file contain all the factories of application
 * 
 */
(function () {
    'use strict';

    /**
     * TODosFactory is used to store the todo tasks as a singleton
     * it contains list of todo tasks and completed tasks
     * it also contains method for adding, getting, setting, etc...
     * 
     */
    angular.module('app').factory('toDosFactory', toDosFactory);

    toDosFactory.$inject = [];
    
    function toDosFactory() {
        var toDosFactory = {};

        // stores list of todo tasks
        toDosFactory.list = [];
        
        // stores list of Completed tasks
        toDosFactory.completedList = [];

        /**
         * Add method is used to add new todo task in list
         * it first count total records and then gets last todo list id
         * add one to it and then push in todo list array
         * 
         * @param {object} toDo TODO object in json
         */
        toDosFactory.add = function (toDo) {
            var count = toDosFactory.list.length;
            var nextId = 1;
            if (count > 0) {
                nextId = toDosFactory.list[(count - 1)].id + 1;
            }
            toDo.id = nextId;
            toDosFactory.list.push(toDo);
        };

        /**
         * This method is used to get all todo tasks from list
         * 
         * @return {Array|app.factoriesL#5.toDosFactory.toDosFactory.list|toDosFactory.list}
         */
        toDosFactory.getToDos = function () {
            return toDosFactory.list;
        };
        
        /**
         * This method is used to set the todo tasks into list
         * it takes array of todos as parameter and assign it to
         * factory todo list array
         * 
         * @param {object array} toDos TODOS object array contain data of todos
         */
        toDosFactory.setToDos = function (toDos) {
            toDosFactory.list = toDos;
        };

        /**
         * This method is used to move todo task in completed task array
         * it search particular task from list and push that task in array of completed tasks
         * and then removes particular task from todo list
         * 
         * @param {integer} id of the todo task
         */
        toDosFactory.moveToCompleted = function (id) {
            for (var i = 0; i < toDosFactory.list.length; i++) {
                if (toDosFactory.list[i].id == id) {
                    toDosFactory.completedList.push(toDosFactory.list[i]);
                    toDosFactory.list.splice(i, 1);
                }
            }
        };
        
        /**
         * This method is used to undo todo task from completed task array
         * it search particular task from completed task list and push that task back to array of todo tasks
         * and then removes particular task from completed task list
         * 
         * @param {integer} id of the todo task
         */
        toDosFactory.undoCompletedTask = function (id) {
            for (var i = 0; i < toDosFactory.completedList.length; i++) {
                if (toDosFactory.completedList[i].id == id) {
                    toDosFactory.add(toDosFactory.completedList[i]);
                    toDosFactory.completedList.splice(i, 1);
                }
            }
        };

        return toDosFactory;
    }
})();