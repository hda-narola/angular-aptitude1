(function () {
    'use strict';

    angular.module('app').run(run);

    run.$inject = ['$rootScope', '$http', 'toDosFactory'];

    function run($rootScope, $http, toDosFactory) {
        // base url to access absoulte path for project
        $rootScope.base_url = "http://localhost:3030/";
        
        /**
         * It gets record from api and dumps in factory of todo tasks
         * 
         */
        $http.get($rootScope.base_url + 'api/todos')
                .then(function (response) {
                    toDosFactory.setToDos(response.data);
                }, function (response) {
                    console.log("Error Please Try Again Later!");
                });

    }
})();