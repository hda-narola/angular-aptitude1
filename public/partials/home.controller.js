(function () {
    'use strict';
    angular.module('app').controller('homeController', homeController);

    homeController.$inject = ['$http', '$rootScope', 'toDosFactory'];

    function homeController($http, $rootScope, toDosFactory) {
        /*jshint validthis: true */
        var vm = this;
        
        // it is stores todo tasks for display
        vm.toDos = [];
        
        // it is stores completed tasks for display
        vm.completedToDos = [];
        
        // flag variable for show and hide completed tasks
        vm.showCompletedTasks = false;

        /**
         * This function is used to get all todos from factory 
         * and set it to display on page
         * 
         */
        vm.getToDos = function () {
            vm.toDos = toDosFactory.list;
            vm.completedToDos = toDosFactory.completedList;
        };

        /**
         * This function is used to mark task as complete
         * it calls moveToCompleted method of factory with id of task
         * 
         * @param {integer} id of todo task
         */
        vm.taskCompleted = function (id) {
            toDosFactory.moveToCompleted(id);
        };

        /**
         * This function is used to Undo completed task back to todo task
         * it calls undoCompletedTask method of factory with id of completed task
         * 
         * @param {integer} id of todo task
         */
        vm.completedTaskUndo = function (id) {
            toDosFactory.undoCompletedTask(id);
        };
        
        /**
         * This function is used to toggle the completed task table
         * for hiding and showing
         */
        vm.toggleCompletedTasks = function () {
            vm.showCompletedTasks = !vm.showCompletedTasks;
        }
    }
})();