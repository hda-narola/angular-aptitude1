(function () {
    'use strict';
    angular.module('app').controller('addController', addController);

    addController.$inject = ['$state', 'toDosFactory'];

    function addController($state, toDosFactory) {
        /*jshint validthis: true */
        var vm = this;
        
        // It is the new object which is bind with form of add task
        vm.toDo = {
            id: 0,
            name: "",
            deadline: "",
            importance: ""
        };

        /**
         * This method is used to add new task in todo tasks
         * by factory add method
         * 
         */
        vm.save = function () {
            toDosFactory.add(vm.toDo);
            $state.go('home');
        };
    }
})();