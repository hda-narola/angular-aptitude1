# README #

This is an aptitude test for Angular 1.5. The assignment is to write a simple TODO application. 

### Prerequisites / rules ###

* You need a proper setup of a modern Node.JS (>v5) and grunt
* The UI does NOT need to look beautiful, design is not evaluated in the assignment - but it should use bootstrap for layout & default appearance (`col-md*`, `form-control`, `jumbotron`, et.c..)  
* The primary goal of the assignment is angular development, not the Node.JS server development. It is permitted to make changes to the server end too if needed though. 
* The TODO data does not need to be persisted, an in-memory solution that clears on server restart is good enough. Do not implement database support.
* The todo list should be stored on the Node.JS server
* There is a ready made API function that returns a static list of TODO:s that you can use:
    * `/api/todos`
* The application should be developed with the mindset of creating a big application using proper separation of concern, modularization / dividing code and html into proper sections 

### How to start ###

* Clone this repository
* Run `npm install`
* Run `grunt`
* Access the server on `http://localhost:3030`

### Assignment ###

To build a TODO list application where you should be able to:

* Add a page header that should remain the same on all pages. It should say "Welcome to the TODO application!"
* Add a page footer that should remain the same on all pages. It should say "No rights reserved."
* Show a list of todo:s, one on each line
    * Each line should show the columns "name", "deadline" (no days left) and "importance" (value of 1-5)
    * Mark todo:s expiring today with yellow
    * Mark todo:s already expired with red
* Add a new todo using a form on a separate page
* Mark a todo as completed

### Submission ###

To submit your assignment, you can fork the repository into your own bitbucket repository and send the link to the finished repository - or you can zip the project and email to [joakim.ahlen@digistrada.com](mailto:joakim.ahlen@digistrada.com). 

The ambition should be to submit your assignment within less than 24 hours of receiving it, but there might be valid reasons for a longer time.

Good luck!